package kz.compass.group.menukz;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import androidx.annotation.Nullable;



public class FullScreenVideoActivity extends Activity {
    VideoView videoView;
    RelativeLayout controlLayout;
    ImageButton seekForward, playBtn, seekBack;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullscreen_video);
        videoView = (VideoView) findViewById(R.id.fullscreen_video_view);
        controlLayout = (RelativeLayout) findViewById(R.id.controlLayout);
        seekForward = (ImageButton) findViewById(R.id.seek_back_btn);
        playBtn = (ImageButton)findViewById(R.id.play_btn);
        seekBack = (ImageButton)findViewById(R.id.seek_forword_btn);
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        int currentPosition = intent.getIntExtra("currentPosition", 0);
        Log.d("url", url);
        videoView.setVideoPath(url);
        videoView.seekTo(currentPosition);
        videoView.start();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }
    public void makeControlVisible(View v){
        controlLayout.setVisibility(View.VISIBLE);
    }
    public void closeVideo(View v){
        videoView.pause();
        finish();
    }
    public void playAndPause(View v){
        if(videoView.isPlaying()) {
            videoView.pause();
            playBtn.setImageResource(R.drawable.ic_rounded_pause_button);
        }
        else{
            videoView.start();
            playBtn.setImageResource(R.drawable.ic_play_button);
        }
    }
    public void seekForward(View v){
        int currentPosition = videoView.getCurrentPosition();
        videoView.seekTo(currentPosition+1500);
    }
    public void seekBack(View v){
        int currentPosition = videoView.getCurrentPosition();
        videoView.seekTo(currentPosition-1500);
    }
    public void makeControlInvisible(View v){
        controlLayout.setVisibility(View.INVISIBLE);
    }
}
