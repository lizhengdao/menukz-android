//================================================================================================================================
//
// Copyright (c) 2015-2019 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
// EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
// and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//================================================================================================================================

package kz.compass.group.menukz;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.util.Log;
import android.app.Activity;
import java.util.HashMap;
import java.util.concurrent.ScheduledExecutorService;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;


import androidx.core.content.ContextCompat;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;

import cn.easyar.CameraDevice;
import cn.easyar.Engine;
import cn.easyar.ImageTracker;
import cn.easyar.VideoPlayer;
import kz.compass.group.menukz.QR.BottomDialogQRFound;
import kz.compass.group.menukz.QR.Restaurant;


public class ARActivity extends Activity
{
    /*
    * Steps to create the key for this sample:
    *  1. login www.easyar.com
    *  2. create app with
    *      Name: HelloARVideo
    *      Package Name: cn.easyar.samples.helloarvideo
    *  3. find the created item in the list and show key
    *  4. set key string bellow
    */
    private static String key = "CrfVCA6kzRQWwv9xhacE4gACHM4Yte2Z83D9czqF4yMOleU+Opjyc3XU4TAjn/5/JIzGNiKX7z1hlek8bdqkPC6F8jQ9veMoBpKka37apD0mleM/PJP1c3Wt/XMtg+g1I5PPNTzUvAoS2qQnLoTvMCGC9XN1raQyIJvrJCGf8ihtq6pzP5rnJSmZ9Dw81LwKbYHvPyuZ8SJt2qQ8LpWkDGPU6z4rg+o0PNS8Cm2F4z88k6gYIpfhNBuE5zIkn+g2bdqkIiqY9TRhteo+OpLUNCyZ4T8mgu8+IdSqczyT6CIq2NQ0LJn0NSaY4XNj1PU0IYXjfwCU7DQsgtIjLpXtOCGRpH1theM/PJOoAjqE4DAsk9IjLpXtOCGRpH1theM/PJOoAj+X9CIqpfYwO5/nPQKX9nNj1PU0IYXjfwKZ8jggmNIjLpXtOCGRpH1theM/PJOoFSqY9TQchuclJpfqHC6GpH1theM/PJOoEg6y0iMule04IZGkDGPU4yk/n/Q0G5/rNByC5zw/1Lw/OprqfW2f9R0glec9bczgMCOF4yxjjaQzOpjiPSq/4iJtzN1zJIyoMiCb9jA8hag2PZnzIWGb4z86nfxzEtqkJy6E7zAhgvVzda2kMiCb6yQhn/Iobauqcz+a5yUpmfQ8PNS8Cm2X6DU9me81bauqcyKZ4iQjk/Vzda2kIiqY9TRhv+swKJPSIy6V7TghkaR9bYXjPzyTqBIjmfM1HZPlPiiY7yUmmehzY9T1NCGF438dk+U+PZLvPyjUqnM8k+giKtjJMyWT5SUbhOcyJJ/oNm3apCIqmPU0YaXzIymX5TQbhOcyJJ/oNm3apCIqmPU0YaX2MD2F4wI/l/I4LprLMD/UqnM8k+giKtjLPjuf6T8bhOcyJJ/oNm3apCIqmPU0YbLjPzyT1SEugu8wI7vnIW3apCIqmPU0YbXHFRuE5zIkn+g2bauqcyqO9jg9k9I4IpPVJS6b9nN1mPM9I9qkODy66TIumqRrKZfqIiqLqiptlPM/K5rjGCuFpGsU1O0rYZXpPD+X9SJhkfQ+OoaoPCqY8zo11Nt9bYDnIyaX6CU81LwKbZXpPCKD6Dg7j6QMY9T2PS6C4D49m/Vzda2kOCCFpAxj1Os+K4PqNDzUvAptheM/PJOoGCKX4TQbhOcyJJ/oNm3apCIqmPU0YbXqPjqS1DQsmeE/JoLvPiHUqnM8k+giKtjUNCyZ9DUmmOFzY9T1NCGF438AlOw0LILSIy6V7TghkaR9bYXjPzyTqAI6hOAwLJPSIy6V7TghkaR9bYXjPzyTqAI/l/QiKqX2MDuf5z0Cl/ZzY9T1NCGF438CmfI4IJjSIy6V7TghkaR9bYXjPzyTqBUqmPU0HIbnJSaX6hwuhqR9bYXjPzyTqBIOstIjLpXtOCGRpAxj1OMpP5/0NBuf6zQcguc8P9S8Pzqa6n1tn/UdIJXnPW3M4DAjheMsEouzfgscJ/XjS31vbGefyPQ2MadOSUGmD3LKxQpc/GNGlI2MMNLIFXRk3Md0IyS2pgb9xPpISXLutRXtVuNvjat0gF6eCBh+YXDtLn551jC1398TaWBxV2STJCfD5SGnEIBvd8xxH3mZXdOgNIUP9RsxbftyXAaB04dAnv5So62WImwAzo+oUGC1z08WGK+MOJXrh+Hj0BXsbiw0oT/Q4I40L3T1L7PtNXAF5/9RSHc5CStjJccbzEyJOJdrvVqmEXnEYObXN0+TpVfeg6AD+cJgVmJX+gan1txhTMBIwubvp5xKT5n3p8kTICVHdooKOjlnmeyBo1dAc5yArc9P9oZR";
    private GLView glView;
    private HelloAR helloAR;
    VideoView videoView;
    Button playWithoutTargetBtn;
    RelativeLayout contentLayout;
    String lastUrl;
    ImageView videoPreview;
    PlayerView playerView;
    Context context;
    boolean fullscreen = false, playWithoutTarget=false;

    ScheduledExecutorService mScheduledExecutorService;
    int mCurrentposition = 0;
    Runnable UpdateCurrentPosition = new Runnable(){

        @Override
        public void run() {
            mCurrentposition = videoView.getCurrentPosition();
            if(mCurrentposition>1000){
                videoView.setAlpha(1f);
                videoPreview.setVisibility(View.INVISIBLE);
                mScheduledExecutorService.shutdown();
            }
            else{
                videoView.setAlpha(0f);
                videoPreview.setVisibility(View.VISIBLE);
            }
        }
    };


    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ar);
        videoPreview = (ImageView) findViewById(R.id.videoPreview);
        playWithoutTargetBtn = findViewById(R.id.play_without_target_btn);
        contentLayout = findViewById(R.id.contentLayout);
        playerView = findViewById(R.id.player_view);
        context = this;
        lastUrl="";
        playerView.bringToFront();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        videoView = (VideoView) findViewById(R.id.videoView);
        MediaController mediaController = new
                MediaController(this);
        videoView.setMediaController(mediaController);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaController.setAnchorView(videoView);
                videoView.start();
            }
        });
        if (!Engine.initialize(this, key)) {
            Log.e("HelloAR", "Initialization Failed.");
            Toast.makeText(ARActivity.this, Engine.errorMessage(), Toast.LENGTH_LONG).show();
            return;
        }

        if (!CameraDevice.isAvailable()) {
            Toast.makeText(ARActivity.this, "CameraDevice not available.", Toast.LENGTH_LONG).show();
            return;
        }
        if (!ImageTracker.isAvailable()) {
            Toast.makeText(ARActivity.this, "ImageTracker not available.", Toast.LENGTH_LONG).show();
            return;
        }
        if (!VideoPlayer.isAvailable()) {
            Toast.makeText(ARActivity.this, "VideoPlayer not available.", Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = getIntent();
        Gson gson = new Gson();
        Restaurant restaurant = gson.fromJson(intent.getStringExtra(BottomDialogQRFound.EXTRA_MESSAGE), Restaurant.class);
        if(restaurant!=null){
            glView = new GLView(this, restaurant);
        }
        else{
            glView = new GLView(this);
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        glView.setOnTargetFoundLister(new OnTargetFoundLister() {

            @Override
            public void onFound() {
                String url = glView.getHelloAR().getCurrentVideoUrl();

                if(videoView!=null && url!=null) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                contentLayout.setVisibility(View.VISIBLE);
                                playerView.setVisibility(View.VISIBLE);
                                videoPreview.bringToFront();
                                playerView.bringToFront();


                                if(!lastUrl.equals(url)){

                                    if(playerView.getPlayer()!=null) playerView.getPlayer().stop(true);
                                    lastUrl = url;
                                    initializePlayer(url);
                                    playWithoutTarget=true;
                                    playWithoutTargetBtn.setText(R.string.continue_scan);
                                }
                                if(!playerView.getPlayer().isPlaying() && !playWithoutTarget){
                                    playerView.getPlayer().setPlayWhenReady(true);
                                }
                            }
                        });
                }
            }

            @Override
            public void onLost() {
 
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!playWithoutTarget) {
                            if (videoView.isPlaying()) videoView.pause();
                            if(playerView.getPlayer().isPlaying()) playerView.getPlayer().setPlayWhenReady(false);
                            contentLayout.setVisibility(View.INVISIBLE);
                            playerView.setVisibility(View.INVISIBLE);
                        }
                    }
                });

            }
        });

        requestCameraPermission(new PermissionCallback() {
            @Override
            public void onSuccess() {
                ((ViewGroup) findViewById(R.id.preview)).addView(glView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                videoView.bringToFront();
            }

            @Override
            public void onFailure() {
            }
        });

    }

    public void initializePlayer(String url){
        Uri ssUri = Uri.parse(url);
        DataSource.Factory dataSourceFactory =
                new DefaultHttpDataSourceFactory(Util.getUserAgent(context, "app-name"));

        MediaSource mediaSource =
                new ProgressiveMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(ssUri);

        SimpleExoPlayer player = new SimpleExoPlayer.Builder(context).build();

        player.prepare(mediaSource);
        player.setPlayWhenReady(true);
        playerView.setPlayer(player);

        ImageView fullscreenButton = playerView.findViewById(R.id.exo_fullscreen_icon);
        fullscreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(fullscreen) {
                    contentLayout.setBackgroundColor(ContextCompat.getColor(ARActivity.this, R.color.content_background));
                    fullscreenButton.setImageDrawable(ContextCompat.getDrawable(ARActivity.this, R.drawable.ic_fullscreen_max));
                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                    if(getActionBar() != null){
                        getActionBar().show();
                    }
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) playerView.getLayoutParams();
                    playWithoutTargetBtn.setVisibility(View.VISIBLE);
                    params.width = params.MATCH_PARENT;
                    params.height = params.WRAP_CONTENT;
                    playerView.setLayoutParams(params);
                    fullscreen = false;
                }else{
                    contentLayout.setBackgroundColor(ContextCompat.getColor(ARActivity.this, R.color.black));
                    fullscreenButton.setImageDrawable(ContextCompat.getDrawable(ARActivity.this, R.drawable.ic_fullscreen_min));
                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN
                            |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                            |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                    if(getActionBar() != null){
                        getActionBar().hide();
                    }
                    playWithoutTargetBtn.setVisibility(View.GONE);

                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) playerView.getLayoutParams();
                    params.width = params.MATCH_PARENT;
                    params.height = params.MATCH_PARENT;
                    playerView.setLayoutParams(params);
                    fullscreen = true;
                }
            }
        });
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public void setPlayWithoutTarget(View v){
        if(playWithoutTarget){
            playWithoutTarget=false;
            playWithoutTargetBtn.setText(R.string.play_without_target);
            playerView.setVisibility(View.INVISIBLE);
            contentLayout.setVisibility(View.INVISIBLE);
            if(playerView.getPlayer().isPlaying()) playerView.getPlayer().setPlayWhenReady(false);
        }
        else{
            playWithoutTarget=true;
            playWithoutTargetBtn.setText(R.string.continue_scan);

        }
    }


    private interface PermissionCallback
    {
        void onSuccess();
        void onFailure();
    }
    private HashMap<Integer, PermissionCallback> permissionCallbacks = new HashMap<Integer, PermissionCallback>();
    private int permissionRequestCodeSerial = 0;
    @TargetApi(23)
    private void requestCameraPermission(PermissionCallback callback)
    {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                int requestCode = permissionRequestCodeSerial;
                permissionRequestCodeSerial += 1;
                permissionCallbacks.put(requestCode, callback);
                requestPermissions(new String[]{Manifest.permission.CAMERA}, requestCode);
            } else {
                callback.onSuccess();
            }
        } else {
            callback.onSuccess();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        if (permissionCallbacks.containsKey(requestCode)) {
            PermissionCallback callback = permissionCallbacks.get(requestCode);
            permissionCallbacks.remove(requestCode);
            boolean executed = false;
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    executed = true;
                    callback.onFailure();
                }
            }
            if (!executed) {
                callback.onSuccess();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (glView != null) { glView.onResume(); }
    }

    @Override
    protected void onPause()
    {
        videoView.stopPlayback();
        if (glView != null) { glView.onPause(); }
        super.onPause();
    }
}
