package kz.compass.group.menukz.QR;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;




public class Menu {


    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("video")
    @Expose
    private String video;

    private String photoPath;

    public String getPhotoPath(){
        return photoPath;
    }
    public void setPhotoPath(String path){
        this.photoPath = path;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getPhoto(){
        return photo;
    }
    public void setPhoto(String photo){
        this.photo = photo;
    }
}