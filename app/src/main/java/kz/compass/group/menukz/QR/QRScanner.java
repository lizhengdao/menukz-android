package kz.compass.group.menukz.QR;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.ViewfinderView;
import com.journeyapps.barcodescanner.camera.CameraSettings;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import kz.compass.group.menukz.R;


/**
 * Custom Scannner Activity extending from Activity to display a custom layout form scanner view.
 */
public class QRScanner extends AppCompatActivity implements
        DecoratedBarcodeView.TorchListener {

    private CaptureManager capture;
    private DecoratedBarcodeView barcodeScannerView;
    private ImageButton switchFlashlightButton;
    private ViewfinderView viewfinderView;
    private String lastText;
    private Boolean isFlashOn=false;
    private Date lastTextChangedTime;
    BottomDialogQRFound dialogQRFound;
    CameraSettings cs;

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {

            if(result.getText() == null) {
                return;
            }
            else if(result.getText().equals(lastText)){
                if(lastTextChangedTime==null) return;
                long diff =  new Date().getTime() - lastTextChangedTime.getTime();//as given
                long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);
                Log.d("time_diff", seconds+"");
                if(seconds<=5) return;
            }
            lastTextChangedTime = new Date();
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            dialogQRFound.newInstance(Integer.parseInt(result.getText())).show(getSupportFragmentManager(), "bottomDialog");
// Vibrate for 400 milliseconds
            v.vibrate(200);
            lastText = result.getText();
            barcodeScannerView.setStatusText(result.getText());

        }

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barcode_activity);
        dialogQRFound = new BottomDialogQRFound();
        barcodeScannerView = findViewById(R.id.zxing_barcode_scanner);
        barcodeScannerView.setTorchListener(this);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        switchFlashlightButton = findViewById(R.id.switch_flashlight);


        viewfinderView = findViewById(R.id.zxing_viewfinder_view);
        // if the device does not have flashlight in its camera,
        // then remove the switch flashlight button...
        if (!hasFlash()) {
            switchFlashlightButton.setVisibility(View.GONE);
        }
        barcodeScannerView.decodeContinuous(callback);
        capture = new CaptureManager(this, barcodeScannerView);
        Collection<BarcodeFormat> formats = Arrays.asList(BarcodeFormat.QR_CODE, BarcodeFormat.CODE_39);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.setShowMissingCameraPermissionDialog(false);
        cs = barcodeScannerView.getCameraSettings();
//        dialogQRFound.newInstance(Integer.parseInt()).show(getSupportFragmentManager(), "bottomDialog");
        Log.d("changed_camera_id", cs.getRequestedCameraId()+"");
        changeMaskColor(null);
        changeLaserVisibility(true);
    }
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        cs.setRequestedCameraId(savedInstanceState.getInt("camera"));
        barcodeScannerView.setCameraSettings(cs);
    }
    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("camera", cs.getRequestedCameraId());
        capture.onSaveInstanceState(outState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    /**
     * Check if the device's camera has a Flashlight.
     * @return true if there is Flashlight, otherwise false.
     */
    private boolean hasFlash() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }
    public void switchCamera(View v){
        cs = barcodeScannerView.getCameraSettings();
        Log.d("cameraId", cs.getRequestedCameraId()+"");
        if(cs.getRequestedCameraId()== Camera.CameraInfo.CAMERA_FACING_FRONT){
            cs.setRequestedCameraId(Camera.CameraInfo.CAMERA_FACING_BACK);
        }
        else{
            cs.setRequestedCameraId(Camera.CameraInfo.CAMERA_FACING_FRONT);
        }
        Log.d("cameraId", cs.getRequestedCameraId()+"");
        barcodeScannerView.setCameraSettings(cs);
        this.recreate();

    }
    public void switchFlashlight(View view) {
        if(isFlashOn){
;            switchFlashlightButton.setAlpha(1f);
            barcodeScannerView.setTorchOff();
            isFlashOn=false;
        }
        else{
            barcodeScannerView.setTorchOn();
            isFlashOn=true;
            switchFlashlightButton.setAlpha(0.8f);
        }
    }

    public void changeMaskColor(View view) {
        Random rnd = new Random();
        int color = Color.argb(90, 39, 38, 38);
        viewfinderView.setMaskColor(color);
    }

    public void changeLaserVisibility(boolean visible) {
        viewfinderView.setLaserVisibility(visible);
    }

    @Override
    public void onTorchOn() {

    }

    @Override
    public void onTorchOff() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        capture.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}