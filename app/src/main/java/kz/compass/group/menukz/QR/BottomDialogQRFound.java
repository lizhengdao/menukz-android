package kz.compass.group.menukz.QR;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.OnProgressListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;

import java.io.File;

import kz.compass.group.menukz.ARActivity;

import kz.compass.group.menukz.R;
import kz.compass.group.menukz.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BottomDialogQRFound extends BottomSheetDialogFragment {
    private static final String DESCRIBABLE_KEY = "describable_key";
    private static final String IS_NEED_TO_DOWNLOAD_KEY = "is_needed_to_download";
    public static final String EXTRA_MESSAGE = "kz.compass.menu.restaurantInfo";
    private Restaurant restaurant ;
    private PrefManager prefManager;
    TextView downloadMenuText;
    ImageView downloadMenuBtnImg;
    Gson gson = new Gson();
    long downloadProgress;
    int counter;
    public static BottomDialogQRFound newInstance(Integer restaurantId) {
        BottomDialogQRFound bottomSheetFragment = new BottomDialogQRFound();
        Bundle bundle = new Bundle();
        bundle.putInt(DESCRIBABLE_KEY, restaurantId);
        bottomSheetFragment.setArguments(bundle);
        return bottomSheetFragment ;
    }

    @Override
    public int getTheme() {
        return R.style.BottomSheetDialogTheme;
    }

    private View.OnClickListener downloadListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            downloadProgress = restaurant.getFilesSize();
            for (Menu menu : restaurant.get_menu()
            ) {
                counter ++;
                String directory = getContext().getFilesDir().getAbsolutePath()+"/"+restaurant.getId()+"/";
                File file = new File(getContext().getFilesDir().getAbsolutePath()+"/"+restaurant.getId()+"/");
                File fileName = new File(directory+menu.getId()+".png");
                Log.d("directory", file.exists()+"");


                downloadMenuText.setText("Скачивание 0%");
                if(fileName.exists()){
                    downloadProgress = downloadProgress - fileName.length();
                    long progressPercentage = 100-(downloadProgress*100/restaurant.getFilesSize());
                    Log.d("downloadProgress", "fullSize: "+restaurant.getFilesSize()+" downloadProgress :"+progressPercentage);
                    downloadMenuText.setText("Скачивание "+progressPercentage+" %");
//                    fileName.delete();
                }
               else {
                    int downloadId = PRDownloader.download(menu.getPhoto(), directory, menu.getId() + ".png")
                            .build()
                            .setOnProgressListener(new OnProgressListener() {
                                @Override
                                public void onProgress(Progress progress) {
                                    Log.d("progress", progress + "");
                                    downloadMenuBtnImg.setVisibility(View.GONE);
                                    long progressPercentage = 100 - ((downloadProgress - progress.currentBytes) * 100 / restaurant.getFilesSize());
                                    downloadMenuText.setText("Скачивание " + progressPercentage + " %");
                                }
                            })
                            .start(new OnDownloadListener() {
                                @Override
                                public void onDownloadComplete() {
                                    Log.d("progress", "completed");
                                    menu.setPhotoPath(directory + menu.getId() + ".png");
                                    counter = counter - 1;
                                    if (counter == 0) {
                                        Log.d("progress", "counter 0");
                                        downloadMenuText.setText("Скачивание 100%");
                                        prefManager.setRestaurantDownloaded(restaurant.getId(), true);
                                        Intent intent = new Intent(v.getContext(), ARActivity.class);
                                        Gson gson = new Gson();
                                        String json = gson.toJson(restaurant);
                                        intent.putExtra(EXTRA_MESSAGE, json);
                                        startActivity(intent);
                                    }
                                }

                                @Override
                                public void onError(Error error) {
                                    Log.e("error", "error while downloading");
                                }


                            });
                }
                Log.d("menu "+menu.getId(), menu.getPhoto()+"");
            }

        }
    };
    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_dialog_qr_found, container, false);
        PRDownloader.initialize(v.getContext());
        LinearLayout downloadMenu = v.findViewById(R.id.download_menu_btn);
        LinearLayout content = v.findViewById(R.id.content);
        downloadMenuText = v.findViewById(R.id.download_menu_btn_text);
        TextView title = v.findViewById(R.id.title);
        TextView subtitle = v.findViewById(R.id.sub_title);
        ProgressBar spinner = v.findViewById(R.id.spinner);
        downloadMenuBtnImg = v.findViewById(R.id.download_menu_btn_img);
        spinner.setVisibility(View.VISIBLE);
        content.setVisibility(View.GONE);
        prefManager = new PrefManager(getContext());
        int restaurantId = getArguments().getInt(DESCRIBABLE_KEY, -1);

        if(isNetworkAvailable(getContext())){
            if(restaurantId!=-1){
                NetworkService.getInstance()
                        .getRestaurantApi()
                        .getRestaurantWithID(restaurantId)
                        .enqueue(new Callback<Restaurant>() {
                            @Override
                            public void onResponse(@NonNull Call<Restaurant> call, @NonNull Response<Restaurant> response) {
                                restaurant = response.body();
                                Log.d("response", response.body()+"");
                                spinner.setVisibility(View.GONE);
                                content.setVisibility(View.VISIBLE);
                                downloadMenuBtnImg.setVisibility(View.VISIBLE);
                                if(response.code()==404){
                                    Log.d("response", response.code()+"");
                                    title.setText(R.string.error_while_scanning_title);
                                    subtitle.setText(R.string.error_while_scanning_subtitle);
                                    downloadMenuText.setText(R.string.error_while_scanning_btn);
                                    Drawable btn_img = getResources().getDrawable(R.drawable.ic_maximize);
                                    downloadMenuBtnImg.setImageDrawable(btn_img);
                                    downloadMenu.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dismiss();
                                        }
                                    });
                                }
                                if(restaurant!=null){
                                    Log.d("restaurant", restaurant.get_menu().size()+"");
                                    title.setText(restaurant.getName());
                                    subtitle.setText(restaurant.getDescription());
                                    int restaurantVersion = prefManager.getRestaurantVersion(restaurant.getId());
                                    boolean isDownloaded = prefManager.isRestaurantDownloaded(restaurant.getId());
                                    if(isDownloaded && restaurantVersion==restaurant.getVersion()){
                                        downloadMenuText.setText(R.string.go_to_restaurant);
                                        downloadMenuBtnImg.setVisibility(View.GONE);
                                        downloadMenu.setOnClickListener(new View.OnClickListener(){
                                            @Override
                                            public void onClick(View v) {

                                                for (Menu menu : restaurant.get_menu()
                                                ) {
                                                    counter++;
                                                    String directory = getContext().getFilesDir().getAbsolutePath() + "/" + restaurant.getId() + "/";
                                                    File file = new File(getContext().getFilesDir().getAbsolutePath() + "/" + restaurant.getId() + "/");
                                                    menu.setPhotoPath(directory + menu.getId() + ".png");
                                                }
                                                Intent intent = new Intent(v.getContext(), ARActivity.class);
                                                Gson gson = new Gson();
                                                String json = gson.toJson(restaurant);
                                                intent.putExtra(EXTRA_MESSAGE, json);
                                                startActivity(intent);
                                            }
                                        });
                                    }
                                    else{
                                        prefManager.setRestaurantVersion(restaurant.getId(), restaurant.getVersion());
                                        prefManager.setRestaurantDownloaded(restaurant.getId(), false);
                                        downloadMenu.setOnClickListener(downloadListener);
                                    }
                                }
//
                            }



                            @Override
                            public void onFailure(@NonNull Call<Restaurant> call,@NonNull Throwable t) {
                                Log.e("error", "error while reading qr");
                                t.printStackTrace();
                            }
                        });
            }
        }
        else{
            spinner.setVisibility(View.GONE);
            content.setVisibility(View.VISIBLE);
            title.setText(R.string.error_internet_connection_title);
            subtitle.setText(R.string.error_internet_connection_subtitle);
            downloadMenuText.setText("Закрыть");
            downloadMenuBtnImg.setVisibility(View.GONE);
            downloadMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.exit(0);
                }
            });
        }
        return v;
    }

}
