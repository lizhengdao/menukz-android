package kz.compass.group.menukz;

public interface OnTargetFoundLister {
    void onFound();
    void onLost();
}
