package kz.compass.group.menukz.QR;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface RestaurantApi {
    @Headers("Authorization: Token 5ac771475ac38b3e831213fbf2251178c17e8ce4")
    @GET("/restaurant/{id}")
    public Call<Restaurant> getRestaurantWithID(@Path("id") int id);
}
