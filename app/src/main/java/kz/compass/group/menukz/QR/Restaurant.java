package kz.compass.group.menukz.QR;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;





public class Restaurant {


    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("version")
    @Expose
    private int version;

    @SerializedName("files_size")
    @Expose
    private int filesSize;

    private Boolean isDownloaded;

    @SerializedName("menu")
    @Expose
    private List<Menu> _menu = new ArrayList<Menu>();


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {return version;}
    public void setVersion(int version) {this.version = version;}

    public String getDescription(){return description;}
    public void setDescription(String description) { this.description = description;}

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public void set_menu(ArrayList<Menu> menu){
        this._menu = menu;
    }
    public List<Menu> get_menu(){
        return _menu;
    }

    public int getFilesSize(){return filesSize;}
    public void setFilesSize(int filesSize){
        this.filesSize = filesSize;
    }

    public Boolean getIsDownloaded(){
        return isDownloaded;
    }
    public void setIsDownloaded(Boolean isDownloaded){
        this.isDownloaded = isDownloaded;
    }

}